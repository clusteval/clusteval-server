/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.framework;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.BindException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ExportException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.FileAppender;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.InvalidRepositoryException;
import de.clusteval.framework.repository.MyRengine;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryAlreadyExistsException;
import de.clusteval.framework.repository.RepositoryCouldNotBeMigratedException;
import de.clusteval.framework.repository.RepositoryVersionTooNewException;
import de.clusteval.framework.repository.RepositoryVersionTooOldException;
import de.clusteval.framework.repository.config.RepositoryConfig;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.db.DatabaseException;
import de.clusteval.framework.threading.RepositorySupervisorThread;
import de.clusteval.framework.threading.RunSchedulerThread;
import de.clusteval.serverclient.IBackendServer;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.MyHighlightingCompositeConverter;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * This class represents the server of the backend of the framework. The server
 * takes commands from the client like performing, resuming or terminating runs,
 * shutdown the framework or get status information about various objects
 * available in the repository (e.g. datasets, runs, programs,...).
 * 
 * <p>
 * You can start the server by invoking the {@link #main(String[])} method. If
 * you do so, you can pass either a path to an existing repository or a new
 * repository is automatically created in the subfolder 'repository'.
 * 
 * <p>
 * When the server is started it registers itself in the RMI registry (remote
 * method invocation), either with the default port 1099 or if specified with
 * -hostport xxxx under any other port.
 * 
 * <p>
 * The start of the server requires a running Rserve instance. If this cannot be
 * found, the server will not start.
 * 
 * @author Christian Wiwie
 */
public class ClustevalBackendServer extends AbstractClustevalServer {

	/**
	 * This variable holds the command line options of the backend server.
	 */
	public static Options serverCLIOptions = new Options();

	static {
		// read properties file with version number
		Properties prop = new Properties();
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream stream = loader.getResourceAsStream("server.date");
		try {
			prop.load(stream);
			VERSION_INFO_STRING = "Jar built: " + prop.getProperty("build.date")
					+ "\nVersion: " + prop.getProperty("build.version");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

		// init valid command line options
		OptionBuilder.withArgName("absRepositoryPath");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription("The absolute path to the repository");
		Option optionAbsRepoPath = OptionBuilder.create("absRepoPath");
		serverCLIOptions.addOption(optionAbsRepoPath);

		OptionBuilder.withDescription("Print this help and usage information");
		Option optionHelp = OptionBuilder.create("help");
		serverCLIOptions.addOption(optionHelp);

		OptionBuilder.withDescription("Print the version of the server");
		Option optionVersion = OptionBuilder.create("version");
		serverCLIOptions.addOption(optionVersion);

		OptionBuilder.withArgName("port");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription("The port this server should listen on");
		Option optionServerPort = OptionBuilder.create("port");
		serverCLIOptions.addOption(optionServerPort);

		OptionBuilder.withArgName("level");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"The verbosity this server should use during the logging process. 0=ALL, 1=TRACE, 2=DEBUG, 3=INFO, 4=WARN, 5=ERROR, 6=OFF");
		OptionBuilder.withType(Integer.class);
		Option optionLogLevel = OptionBuilder.create("logLevel");
		serverCLIOptions.addOption(optionLogLevel);

		OptionBuilder.withArgName("number");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"The maximal number of threads that should be created in parallel when executing runs.");
		OptionBuilder.withType(Integer.class);
		Option optionNoOfThreads = OptionBuilder.create("numberOfThreads");
		serverCLIOptions.addOption(optionNoOfThreads);

		OptionBuilder.withArgName("check");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"Indicates, whether this server should check for run results in its repository.");
		OptionBuilder.withType(Boolean.class);
		Option checkForRunResults = OptionBuilder.create("checkForRunResults");
		serverCLIOptions.addOption(checkForRunResults);

		OptionBuilder.withDescription(
				"Indicates, whether this server should connect to a database.");
		Option noDatabase = OptionBuilder.create("noDatabase");
		serverCLIOptions.addOption(noDatabase);

		OptionBuilder.withArgName("rServeHost");
		OptionBuilder.hasArg();
		OptionBuilder
				.withDescription("The address on which Rserve is listening.");
		OptionBuilder.withType(String.class);
		Option rServeHost = OptionBuilder.create("rServeHost");
		serverCLIOptions.addOption(rServeHost);

		OptionBuilder.withArgName("rServePort");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription("The port on which Rserve is listening.");
		OptionBuilder.withType(Integer.class);
		Option rServePort = OptionBuilder.create("rServePort");
		serverCLIOptions.addOption(rServePort);

		OptionBuilder.withArgName("noR");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"Whether not to connect to R. If yes, no connection will be established.");
		OptionBuilder.withType(Boolean.class);
		Option noR = OptionBuilder.create("noR");
		serverCLIOptions.addOption(noR);

		OptionBuilder.withArgName("administrators");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"A comma separated list of UUIDs of administrators.");
		OptionBuilder.withType(String.class);
		Option administrators = OptionBuilder.create("administrators");
		serverCLIOptions.addOption(administrators);

		OptionBuilder.withDescription(
				"By default, ClustEval will error out if the repository version is incompatible with this version of ClustEval. This flag indicates, that instead the repository should be migrated to a newer version if needed.");
		Option migrateRepository = OptionBuilder.create("migrateRepository");
		serverCLIOptions.addOption(migrateRepository);

		OptionBuilder.withDescription(
				"By default, ClustEval will error out if a database should be used and the database schema is not initialized. This flag indicates, that instead ClustEval should wait until the database schema is initialized.");
		Option dbSchema = OptionBuilder
				.create("awaitInitializedDatabaseSchema");
		serverCLIOptions.addOption(dbSchema);

		OptionBuilder.withDescription(
				"By default, ClustEval will error out if a database should be used and the database server is not available. This flag indicates, that instead ClustEval should wait until the database server is available.");
		Option dbServer = OptionBuilder.create("awaitStartedDatabaseServer");
		serverCLIOptions.addOption(dbServer);
	}

	protected static String VERSION_INFO_STRING;

	/**
	 * Instantiates a new backend server.
	 * 
	 * @param absRepositoryPath
	 *            The absolute path to the repository used by this server.
	 * @throws FileNotFoundException
	 * @throws InvalidRepositoryException
	 * @throws RepositoryAlreadyExistsException
	 * @throws RepositoryConfigurationException
	 * @throws RepositoryConfigNotFoundException
	 * @throws InterruptedException
	 * @throws DatabaseException
	 * @throws RepositoryVersionTooNewException
	 * @throws RepositoryVersionTooOldException
	 * @throws RepositoryCouldNotBeMigratedException
	 */
	public ClustevalBackendServer(final String absRepositoryPath)
			throws FileNotFoundException, RepositoryAlreadyExistsException,
			InvalidRepositoryException, RepositoryConfigNotFoundException,
			RepositoryConfigurationException, InterruptedException,
			DatabaseException, RepositoryVersionTooOldException,
			RepositoryVersionTooNewException,
			RepositoryCouldNotBeMigratedException {
		this(new Repository(absRepositoryPath, null));
	}

	/**
	 * Instantiates a new backend server and registers the server at the RMI
	 * registry.
	 * 
	 * @param repository
	 *            The repository used by this server.
	 * @throws InterruptedException
	 * @throws DatabaseException
	 */
	public ClustevalBackendServer(final IRepository repository)
			throws InterruptedException, DatabaseException {
		this(repository, true);
	}

	/**
	 * @param repository
	 * @param registerServer
	 * @throws InterruptedException
	 * @throws DatabaseException
	 */
	public ClustevalBackendServer(final IRepository repository,
			final boolean registerServer)
			throws InterruptedException, DatabaseException {
		super(repository, registerServer);
	}

	/**
	 * This method can be used to start a backend server. The args parameter can
	 * contain options that specify the behaviour of the server:
	 * <ul>
	 * <li><b>-absRepositoryPath <absRepoPath></b>: An absolute path to the
	 * repository</li>
	 * <li><b>-port <port></b>: The port on which this server should listen</li>
	 * </ul>
	 * 
	 * @param args
	 *            Arguments to control the behaviour of the server
	 * @throws FileNotFoundException
	 * @throws InvalidRepositoryException
	 * @throws RepositoryAlreadyExistsException
	 * @throws RepositoryConfigurationException
	 * @throws RepositoryConfigNotFoundException
	 * @throws InterruptedException
	 * @throws DatabaseException
	 */
	public static void main(String[] args) throws FileNotFoundException,
			RepositoryAlreadyExistsException, InvalidRepositoryException,
			RepositoryConfigNotFoundException, RepositoryConfigurationException,
			InterruptedException, DatabaseException {

		// bugfix for log4j warning
		org.apache.log4j.Logger.getRootLogger()
				.setLevel(org.apache.log4j.Level.OFF);
		org.apache.log4j.Logger.getRootLogger()
				.addAppender(new org.apache.log4j.ConsoleAppender(
						new org.apache.log4j.PatternLayout(
								"%d %-5p %c - %F:%L - %m%n")));

		CommandLineParser parser = new PosixParser();
		try {
			CommandLine cmd = parser.parse(serverCLIOptions, args);

			if (cmd.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("clustevalServer",
						"clusteval backend server " + VERSION_INFO_STRING,
						serverCLIOptions, "");
				System.exit(0);
			}

			if (cmd.hasOption("version")) {
				System.out.println(VERSION_INFO_STRING);
				System.exit(0);
			}

			if (cmd.getArgList().size() > 0)
				throw new ParseException("Unknown parameters: "
						+ Arrays.toString(cmd.getArgs()));

			if (cmd.hasOption("port"))
				port = Integer.parseInt(cmd.getOptionValue("port"));
			else
				port = 1099;

			initLogging(cmd);

			if (cmd.hasOption("numberOfThreads"))
				RunSchedulerThread.NUMBER_THREADS = Integer
						.parseInt(cmd.getOptionValue("numberOfThreads"));

			if (cmd.hasOption("checkForRunResults"))
				RepositorySupervisorThread.CHECK_FOR_RUN_RESULTS = Boolean
						.parseBoolean(cmd.getOptionValue("checkForRunResults"));

			if (cmd.hasOption("noDatabase"))
				// config.setNoDatabase(true);
				RepositoryConfig.NO_DATABASE = true;

			if (cmd.hasOption("rServeHost"))
				MyRengine.R_SERVE_HOST = cmd.getOptionValue("rServeHost");

			if (cmd.hasOption("rServePort"))
				MyRengine.R_SERVE_PORT = Integer
						.parseInt(cmd.getOptionValue("rServePort"));

			if (cmd.hasOption("noR"))
				config.noR = cmd.getOptionValue("noR").equals("true");

			if (cmd.hasOption("administrators"))
				config.administratorUUIDs = new HashSet<String>(
						Arrays.asList(cmd.getOptionValues("administrators")));

			if (cmd.hasOption("migrateRepository"))
				Repository.MIGRATE_REPOSITORY = true;

			if (cmd.hasOption("awaitInitializedDatabaseSchema"))
				Repository.AWAIT_INITIALIZED_DATABASE_SCHEMA = true;

			if (cmd.hasOption("awaitStartedDatabaseServer"))
				Repository.AWAIT_STARTED_DATABASE_SERVER = true;

			System.out.println("Starting ClustEval server");
			System.out.println(VERSION_INFO_STRING);
			System.out.println("=========================");

			MyRengine.initializeRConnection();

			if (cmd.hasOption("absRepoPath"))
				new ClustevalBackendServer(cmd.getOptionValue("absRepoPath"));
			else
				new ClustevalBackendServer(
						new File("repository").getAbsolutePath());

		} catch (ParseException e1) {
			System.err.println("Parsing failed.  Reason: " + e1.getMessage());

			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("clustevalServer",
					"clusteval backend server " + VERSION_INFO_STRING,
					serverCLIOptions, "");
		} catch (RepositoryVersionTooOldException e) {
			Logger log = LoggerFactory.getLogger(ClustevalBackendServer.class);
			log.error(e.getMessage());
		} catch (RepositoryVersionTooNewException e) {
			Logger log = LoggerFactory.getLogger(ClustevalBackendServer.class);
			log.error(e.getMessage());
		} catch (RepositoryCouldNotBeMigratedException e) {
			Logger log = LoggerFactory.getLogger(ClustevalBackendServer.class);
			log.error(e.getMessage());
		}
	}

	/**
	 * This method is responsible for creating all the appender that are added
	 * to the logger.
	 * <p>
	 * Three appenders are created:
	 * <ul>
	 * <li><b>ConsoleAppender</b>: Writes the logging output to the standard out
	 * </li>
	 * <li><b>FileAppender</b>: Writes the logging output as formatter text to
	 * the file clustevalServer.log</li>
	 * <li><b>FileAppender</b>: Writes the logging output in lilith binary
	 * format to the file clustevalServer.lilith</li>
	 * </ul>
	 * 
	 * @param cmd
	 *            The command line parameters including possible options of
	 *            logging
	 * @throws ParseException
	 */
	private static void initLogging(CommandLine cmd) throws ParseException {
		Logger log = LoggerFactory.getLogger(ClustevalBackendServer.class);

		Level logLevel;
		if (cmd.hasOption("logLevel")) {
			switch (Integer.parseInt(cmd.getOptionValue("logLevel"))) {
				case 0 :
					logLevel = Level.ALL;
					break;
				case 1 :
					logLevel = Level.TRACE;
					break;
				case 2 :
					logLevel = Level.DEBUG;
					break;
				case 3 :
					logLevel = Level.INFO;
					break;
				case 4 :
					logLevel = Level.WARN;
					break;
				case 5 :
					logLevel = Level.ERROR;
					break;
				case 6 :
					logLevel = Level.OFF;
					break;
				default :
					throw new ParseException(
							"The logLevel argument requires one of the value of [0,1,2,3,4,5,6]");
			}
		} else {
			logLevel = Level.INFO;
		}

		ch.qos.logback.classic.Logger logger = ((ch.qos.logback.classic.Logger) LoggerFactory
				.getLogger(Logger.ROOT_LOGGER_NAME));
		logger.setLevel(logLevel);

		ConsoleAppender<ILoggingEvent> consoleApp = (ConsoleAppender<ILoggingEvent>) logger
				.iteratorForAppenders().next();
		consoleApp
				.setContext((LoggerContext) LoggerFactory.getILoggerFactory());
		consoleApp.setEncoder(new PatternLayoutEncoder());
		consoleApp.setWithJansi(true);
		PatternLayout layout = new PatternLayout();
		layout.getDefaultConverterMap().put("highlight",
				MyHighlightingCompositeConverter.class.getName());
		layout.setPattern("@localhost:" + port
				+ " %date{dd MMM yyyy HH:mm:ss.SSS} %highlight([%thread] %-5level %logger{35} - %msg) %n");
		layout.setContext((LoggerContext) LoggerFactory.getILoggerFactory());
		layout.start();
		consoleApp.setLayout(layout);
		consoleApp.start();
		logger.addAppender(consoleApp);

		// file appender for clustevalServer.log plaintext file
		FileAppender<ILoggingEvent> fileApp = new FileAppender<ILoggingEvent>();
		fileApp.setName("serverLogFile");
		String logFilePath = FileUtils.buildPath(System.getProperty("user.dir"),
				"clustevalServer.log");
		fileApp.setFile(logFilePath);

		fileApp.setAppend(true);
		fileApp.setContext((LoggerContext) LoggerFactory.getILoggerFactory());
		fileApp.setEncoder(new PatternLayoutEncoder());
		layout = new PatternLayout();
		layout.setPattern("@localhost:" + port
				+ " %date{dd MMM yyyy HH:mm:ss.SSS} [%thread] %-5level %logger{35} - %msg%n");
		layout.setContext((LoggerContext) LoggerFactory.getILoggerFactory());
		layout.start();
		fileApp.setLayout(layout);
		fileApp.start();
		logger.addAppender(fileApp);

		// file appender for clustevalServer.lilith binary file
		// removed 30.01.2013
		// FileAppender fileAppLilith = new FileAppender();
		// fileAppLilith.setName("serverLogFileLilith");
		// logFilePath = FileUtils.buildPath(System.getProperty("user.dir"),
		// "clustevalServer.lilith");
		// fileAppLilith.setFile(logFilePath);
		//
		// fileAppLilith.setAppend(true);
		// fileAppLilith.setContext((LoggerContext) LoggerFactory
		// .getILoggerFactory());
		// ClassicLilithEncoder encoder = new ClassicLilithEncoder();
		// encoder.setIncludeCallerData(true);
		// fileAppLilith.setEncoder(encoder);
		//
		// fileAppLilith.start();
		// logger.addAppender(fileAppLilith);

		log.debug("Using log level " + logLevel);
	}

	/**
	 * A helper method for {@link #ClusteringEvalFramework(Repository)}, which
	 * registers the new backend server instance in the RMI registry.
	 * 
	 * @param framework
	 *            The backend server to register.
	 * @return True, if the server has been registered successfully
	 */
	protected static boolean registerServer(IBackendServer framework) {
		Logger log = LoggerFactory.getLogger(ClustevalBackendServer.class);

		try {
			LocateRegistry.createRegistry(port);

			IBackendServer stub = (IBackendServer) UnicastRemoteObject
					.exportObject(framework, port);
			Registry registry = LocateRegistry.getRegistry(port);
			registry.bind("EvalServer", stub);
			log.info("Framework up and listening on port " + port);
			log.info("Used number of processors: "
					+ RunSchedulerThread.NUMBER_THREADS);
			return true;
		} catch (ExportException e) {
			if (e.getCause() != null && e.getCause() instanceof BindException) {
				log.error(
						"Could not bind the specified port. Probably another ClustEval instance is already running.");
			}
			return false;
		} catch (AlreadyBoundException e) {
			log.error("Another instance is already running...");
			return false;
		} catch (RemoteException e) {
			log.error(
					"An error occurred when trying to register the backend server in RMI registry:");
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Change the log level of this JVM.
	 * 
	 * @param logLevel
	 *            The new log level
	 */
	public static void logLevel(Level logLevel) {
		((ch.qos.logback.classic.Logger) LoggerFactory
				.getLogger(Logger.ROOT_LOGGER_NAME)).setLevel(logLevel);
	}

	// TODO
	// /*
	// * (non-Javadoc)
	// *
	// * @see
	// *
	// de.clusteval.serverclient.IBackendServer#addBackendServerSlave(java.lang.
	// * String)
	// */
	// @Override
	// public boolean addBackendServerSlave(String host, int port) throws
	// RemoteException {
	// try {
	// Registry registry = LocateRegistry.getRegistry(host, port);
	// IBackendServer server = (IBackendServer) registry.lookup("EvalServer");
	// IRunSchedulerThread runScheduler =
	// this.repository.getSupervisorThread().getRunScheduler();
	// if (runScheduler.isBackendServerSlaveRegistered(server)) {
	// this.log.warn(String.format("Slave computation server already added:
	// '%s:%d'", host, port));
	// return false;
	// }
	// runScheduler.addBackendServerSlave(server);
	// this.log.info(String.format("Added slave computation server: '%s:%d' with
	// %d threads", host, port,
	// server.getThreadNumber()));
	// } catch (ConnectException e) {
	// this.log.error("Could not connect to server");
	// throw e;
	// } catch (RemoteException e) {
	// e.printStackTrace();
	// } catch (NotBoundException e) {
	// e.printStackTrace();
	// }
	//
	// return false;
	// }
	//
	// /*
	// * (non-Javadoc)
	// *
	// * @see
	// * de.clusteval.serverclient.IBackendServer#delicateIteration(java.lang.
	// * String, java.lang.String, java.lang.String,
	// * de.clusteval.program.ParameterSet)
	// */
	// @Override
	// public IClustering delegateIteration(String clientId, String
	// repositoryAbsPath, String dataConfigId,
	// String programConfigId, ParameterSet paramSet) throws RemoteException {
	// // Repository repositoryForExactPath =
	// // Repository.getRepositoryForExactPath(repositoryAbsPath);
	// // SupervisorThread supervisorThread =
	// // repositoryForExactPath.getSupervisorThread();
	// // RunSchedulerThread runScheduler = supervisorThread.getRunScheduler();
	// //
	// // ExecutionIterationWrapper executionIterationWrapper = new
	// // ExecutionIterationWrapper();
	// // ClusteringIterationRunnable executionIterationRunnable = new
	// // ClusteringIterationRunnable(executionIterationWrapper, format);
	// //
	// // runScheduler.registerIterationRunnable(executionIterationRunnable)
	// return null;
	// }

}
