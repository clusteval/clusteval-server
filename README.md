clusteval
=========

[![build status](http://gitlab.compbio.sdu.dk/wiwiec/clusteval/badges/master/build.svg)](http://gitlab.compbio.sdu.dk/wiwiec/clusteval/commits/master)
[![coverage report](http://gitlab.compbio.sdu.dk/wiwiec/clusteval/badges/master/coverage.svg)](http://gitlab.compbio.sdu.dk/wiwiec/clusteval/commits/master)

An Integrated Clustering Evaluation Framework for Cluster Analysis

This repository contains the eclipse java project of clusteval. This includes the sources of the backend server and client.

The eclipse java project for the components that can be loaded by the clusteval server dynamically during runtime is contained in this repository: https://github.com/wiwie/clustevalPackages

The frontend of clusteval including the Ruby on Rails website can be found in: https://github.com/wiwie/clustevalWebsite
